/**
 * 2015年6月16日下午2:22:39   2015
 * @author zhangwei  396033084@qq.com
 * @Description: 
 */
package ne4Spring;

import java.util.LinkedHashMap;

import com.tmsps.ne4spring.orm.ClassUtil;
import com.tmsps.ne4spring.orm.MySQLUtil;

/**
 * @2015年6月16日下午2:22:39
 * @author zhangwei  396033084@qq.com
 * @ClassName : TableBean
 * @Description: 
 */
public class TableBean {
	public static void main(String[] args) {
		TestDataModelBean tdb = new TestDataModelBean();
		tdb.setUserName("aaaa");
		tdb.setUserPassword("bbb");
		
		LinkedHashMap<Object, Object> map = ClassUtil.getClassKeyValNotNullAndPK(tdb);
		System.err.println(map);
		System.err.println(ClassUtil.getKeyList(map));
		System.err.println(ClassUtil.getValList(map));
//		System.err.println(ClassUtil.getClassName(tdb.getClass()));
		
		System.err.println(MySQLUtil.getTemplateInsSQL("tableName",ClassUtil.getKeyList(map)));
		System.err.println(MySQLUtil.getInsSQL(tdb.getClass()));
		
		
//		tdb.setTid("id");
//		tdb.setUserName("张三");
//		System.err.println(ClassUtil.getClassKeyVal(tdb));
//		System.err.println(ClassUtil.getClassKeyValNotNull(tdb));
//		System.err.println(MySQLUtil.getChangeUpdateSQL(tdb));
	}
}
