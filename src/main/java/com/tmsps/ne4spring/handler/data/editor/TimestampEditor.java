package com.tmsps.ne4spring.handler.data.editor;

import java.sql.Timestamp;
import org.springframework.beans.propertyeditors.PropertiesEditor;

/**
 * Timestamp 属性处理
 * 
 * @author zhangwei 396033084@qq.com ON 2015年7月23日 下午2:08:48
 *
 */
public class TimestampEditor extends PropertiesEditor {
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null || text.equals("")) {
			return;
		} else {
			setValue(Timestamp.valueOf(text));
		}
	}
}
