package com.tmsps.ne4spring.utils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * 
 * @author zhangwei 396033084@qq.com
 *
 */
public class CacheUtil {
	private CacheManager cacheManager;

	private CacheUtil() {
		cacheManager = CacheManager.create();
	}

	private static final CacheUtil cacheUtilsInstance = new CacheUtil();

	public static CacheUtil getCacheUtils() {
		return cacheUtilsInstance;
	}

	/**
	 * 对象入缓存
	 * 
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	public void put(String cacheName, String key, Object value) {
		Cache cache = cacheManager.getCache(cacheName);
		Element element = new Element(key, value);
		cache.put(element);
	}
	
	/**
	 * 缓存取对象
	 * 
	 * @return
	 */
	public Object get(String cacheName, String key) {
		Cache cache = cacheManager.getCache(cacheName);
		Element element = cache.get(key);
		if (null != element) {
			return element.getObjectValue();
		}
		return null;
	}

	public void remove(String cacheName, String key) {
		Cache cache = cacheManager.getCache(cacheName);
		cache.remove(key);
	}

	/**
	 * 清空缓存
	 * 
	 * @param cacheName
	 */
	public void removeAll(String cacheName) {
		Cache cache = cacheManager.getCache(cacheName);
		cache.removeAll();
	}
}
