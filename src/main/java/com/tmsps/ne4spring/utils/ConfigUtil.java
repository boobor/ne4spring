package com.tmsps.ne4spring.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 配置文件读取工具
 * 
 * @author zhangwei
 * 
 */
public class ConfigUtil extends Properties {
	private static final long serialVersionUID = 1L;

	/**
	 * 载入配置文件
	 */
	private ConfigUtil() {
		InputStream in = getClass().getClassLoader().getResourceAsStream(
				"config.properties");
		try {
			load(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static ConfigUtil _instance = new ConfigUtil();

	public static ConfigUtil getConfigReader() {
		return _instance;
	}

	/**
	 * 获取值
	 */
	public String get(String key) {
		return this.getProperty(key);
	}
}
