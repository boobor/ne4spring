ne4Spring 是基于spring开发的一套框架，它并不会替代谁，只是将开发中常用的一些东西进行一些封装，从而简化开发流程和工序
```
<dependency>
    <groupId>com.tmsps</groupId>
    <artifactId>ne4Spring</artifactId>
    <version>1.6.2</version>
</dependency>
```

如何使用GeUtil生成Bean
```
package gold;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSON;
import com.tmsps.ne4spring.utils.generator.BeanGenerator;
import com.tmsps.ne4spring.utils.generator.GeUtils;
import com.tmsps.ne4spring.utils.generator.TableMeta;

public class SpringUtils {
    private static BeanFactory bf;

    public static <T extends Object> T getBean(String beanId) {

        bf = new FileSystemXmlApplicationContext("classpath:applicationContext.xml");
        @SuppressWarnings("unchecked")
        T bean = (T) bf.getBean(beanId);
        return bean;
    }

    public static void main(String[] args) throws SQLException {
        long start = System.currentTimeMillis();
        System.err.println("开始生成");
        DruidDataSource ds = SpringUtils.getBean("dataSource");
        GeUtils ge = new GeUtils(ds.getConnection());
        //bean文件输出的文件夹路径
        String outPath = ge.getPath()+ "/src/main/java/com/tmsps/gold/bean";
        System.err.println(outPath);
        List<TableMeta> tableMetas =  ge.getTableMetas();
        System.err.println(JSON.toJSON(tableMetas));
        for (TableMeta tableMeta2 : tableMetas) {
            //bean的package 路径
            new BeanGenerator("com.tmsps.gold.bean").getBeanContent(tableMeta2);
        }
        for (TableMeta tableMeta2 : tableMetas) {
            try {
                ge.wirtToFile(tableMeta2, outPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();
        System.err.println("运行结束，耗时 ："+(end-start)/1000);

    }
}
```